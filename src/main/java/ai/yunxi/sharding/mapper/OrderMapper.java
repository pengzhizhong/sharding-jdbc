package ai.yunxi.sharding.mapper;

import ai.yunxi.sharding.model.Order;
import ai.yunxi.sharding.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
     int save(Order order);

     List<Order> selectHint();

     List<Order> findOrderList(@Param("array") Long[] arr);
}
