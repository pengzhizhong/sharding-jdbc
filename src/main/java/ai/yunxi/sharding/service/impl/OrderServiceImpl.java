package ai.yunxi.sharding.service.impl;

import ai.yunxi.sharding.mapper.OrderItemMapper;
import ai.yunxi.sharding.mapper.OrderMapper;
import ai.yunxi.sharding.model.Order;
import ai.yunxi.sharding.model.OrderItem;
import ai.yunxi.sharding.service.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private OrderItemMapper orderItemMapper;

    @Override
    public void save(Order order, OrderItem item) {
        orderMapper.save(order);
        orderItemMapper.save(item);
    }

    @Override
    public List<Order> findHint() {
        return orderMapper.selectHint();
    }

    @Override
    public List<Order> findOrderList(Long[] arr) {
        return orderMapper.findOrderList(arr);
    }
}
