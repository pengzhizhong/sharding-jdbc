package ai.yunxi.sharding;

import ai.yunxi.sharding.model.ItemGenerator;
import ai.yunxi.sharding.model.Order;
import ai.yunxi.sharding.model.OrderGenerator;
import ai.yunxi.sharding.model.OrderItem;
import ai.yunxi.sharding.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VipShardingApplication.class)
public class ComplexShardingApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(ComplexShardingApplicationTests.class);


    @Autowired
    private OrderService orderService;


    @Test
    public void test() {
        Order order = OrderGenerator.generate();
        order.setUserId(10000010);
        order.setOrderId(1000010);
        OrderItem orderItem = ItemGenerator.generate();
        orderItem.setUserId(order.getUserId());
        orderItem.setOrderId(order.getOrderId());
        logger.info(orderItem.toString());
        orderService.save(order, orderItem);
    }
}
