package ai.yunxi.sharding;

import ai.yunxi.sharding.service.OrderService;
import io.shardingsphere.api.HintManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VipShardingApplication.class)
public class HintApplicationTests {
    @Autowired
    private OrderService orderService;

    @Test
    public void test() {
        // Hint分片策略必须要使用 HintManager工具类
        HintManager hintManager = HintManager.getInstance();
        //
        hintManager.addDatabaseShardingValue("t_order", 0);
        hintManager.addTableShardingValue("t_order", 1);


        // 直接指定对应具体的数据库
        //hintManager.setDatabaseShardingValue(1);
        System.out.println(orderService.findHint());
    }
}
