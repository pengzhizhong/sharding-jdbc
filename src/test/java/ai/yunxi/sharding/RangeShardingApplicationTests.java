package ai.yunxi.sharding;

import ai.yunxi.sharding.model.*;
import ai.yunxi.sharding.service.OrderService;
import ai.yunxi.sharding.service.ProvinceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = VipShardingApplication.class)
public class RangeShardingApplicationTests {
    @Autowired
    private OrderService orderService;

    @Test
    public void test() {
        for (int i = 0; i < 10; i++) {
            Order order = OrderGenerator.generate();
            order.setUserId(10000000 + i);
            order.setOrderId(10000000 + i);
            OrderItem orderItem = ItemGenerator.generate();
            orderItem.setUserId(order.getUserId());
            orderItem.setOrderId(order.getOrderId());
            orderService.save(order, orderItem);
        }
    }

    @Test
    public void seletcTest() {
        Long[] arr = new Long[]{10000000L, 10000001L, 10000002L, 10000005L};
        List<Order> orderList = orderService.findOrderList(arr);
        System.out.println(orderList);
    }
}
