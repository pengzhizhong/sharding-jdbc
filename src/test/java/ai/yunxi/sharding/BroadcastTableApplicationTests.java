package ai.yunxi.sharding;

import ai.yunxi.sharding.model.Province;
import ai.yunxi.sharding.service.ProvinceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 广播表测试
 * 空间换时间，每个数据库的t_province 保存同样的一份数据
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = VipShardingApplication.class)
public class BroadcastTableApplicationTests {
    @Autowired
    private ProvinceService provinceService;

    @Test
    public void test() {
        Province pro = new Province();
        pro.setId(111);
        pro.setName("长沙");
        provinceService.save(pro);
    }
}
